let
  cabal-3_4 =
    fetchGit {
      url = "https://github.com/haskell/cabal";
      rev = "0ee90d0b426d177c94cef9b2e23fe923dfe56508";
      ref = "3.4";
    };

  # Workaround for https://github.com/input-output-hk/haskell.nix/issues/979
  allowCabalReinstall = {
    nonReinstallablePkgs = [
        "rts" "ghc-heap" "ghc-prim" "integer-gmp" "integer-simple" "base"
        "deepseq" "array" "ghc-boot-th" "pretty" "template-haskell"
        # ghcjs custom packages
        "ghcjs-prim" "ghcjs-th"
        "ghc-boot"
        "ghc" "Win32" "array" "binary" "bytestring" "containers"
        "directory" "filepath" "ghc-boot" "ghc-compact" "ghc-prim"
        # "ghci" "haskeline"
        "hpc"
        "mtl" "parsec" "process" "text" "time" "transformers"
        "unix" "xhtml"
        # "stm" "terminfo"
      ];
    };

  buildCabal = { pkgs }:
    let
      proj = pkgs.haskell-nix.project {
          projectFileName = "cabal.project";
          src = cabal-3_4;
          cabalProjectLocal = ''
            package lukko
              flags: -ofd-locking
          '';
          compiler-nix-name = "ghc8102"; # Not required for `stack.yaml` based projects.
          modules =  [ allowCabalReinstall ];
        };
    in proj.cabal-install.components.exes.cabal;

  #haskellNix = import (builtins.fetchTarball "https://github.com/input-output-hk/haskell.nix/archive/master.tar.gz") {};
  haskellNix = import (builtins.fetchTarball "https://github.com/input-output-hk/haskell.nix/archive/0d072acf6e2acb9d5495a489d1e34d8736e5a847.tar.gz") {};
  nixpkgsSrc = haskellNix.sources.nixpkgs-2003;
  pkgs = import nixpkgsSrc haskellNix.nixpkgsArgs;

  armv7l-musl-pkgs = import nixpkgsSrc (haskellNix.nixpkgsArgs // {
    crossSystem = pkgs.lib.systems.platforms.armv7l-hf-multiplatform // { config = "armv7l-unknown-linux-musleabihf"; };
  });

  platforms = {
    i386-musl = buildCabal { pkgs = pkgs.pkgsCross.musl32; };
    amd64 = buildCabal { pkgs = pkgs; };
    amd64-musl = buildCabal { pkgs = pkgs.pkgsCross.musl64; };
    aarch64 = buildCabal { pkgs = pkgs.pkgsCross.aarch64-multiplatform-musl; };
    armv7l = buildCabal { pkgs = pkgs.pkgsCross.armv7l-hf-multiplatform; };
    armv7l-musl = buildCabal { pkgs = armv7l-musl-pkgs; };
    win64 = buildCabal { pkgs = pkgs.pkgsCross.mingwW64; };
  };
in platforms
